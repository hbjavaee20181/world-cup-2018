package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.CrudDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.io.Serializable;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public abstract class JpaCrudDao<T, PK extends Serializable> implements CrudDao<T, PK> {

    protected EntityManagerFactory emf;

    public JpaCrudDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public T create(T t) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction ts = em.getTransaction();
        try {
            ts.begin();
            em.persist(t);
            ts.commit();
            return t;
        } finally {
            if (ts.isActive()) {
                ts.rollback();
            }
            em.close();
        }
    }

    @Override
    public T find(PK id) {
        EntityManager em = emf.createEntityManager();
        try {
            T entity = em.find(getTargetClass(), id);
            return entity;
        } catch (NoResultException e) {
            System.out.println("No result found for id " + id + " and entity " + getTargetClass().getSimpleName());
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public T update(T t) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction ts = em.getTransaction();
        try {
            ts.begin();
            T updatedEntity = em.merge(t);
            ts.commit();
            return updatedEntity;
        } finally {
            if (ts.isActive()) {
                ts.rollback();
            }
            em.close();
        }
    }

    @Override
    public void delete(T t) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction ts = em.getTransaction();
        try {
            ts.begin();
            em.remove(t);
            ts.commit();
        } finally {
            if (ts.isActive()) {
                ts.rollback();
            }
            em.close();
        }
    }

    protected abstract Class<T> getTargetClass();
}
