package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.TvChannelDao;
import com.humanbooster.worldcup2018.entity.TvChannel;

import javax.persistence.EntityManagerFactory;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaTvChannelDao extends JpaCrudDao<TvChannel, Long> implements TvChannelDao {

    public JpaTvChannelDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<TvChannel> getTargetClass() {
        return TvChannel.class;
    }
}
