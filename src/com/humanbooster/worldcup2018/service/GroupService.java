package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018.dao.DaoFactory;
import com.humanbooster.worldcup2018.dao.GroupDao;
import com.humanbooster.worldcup2018.entity.Group;
import com.humanbooster.worldcup2018consumer.model.GroupItem;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class GroupService {

    public Long createGroupIfNotExists(String strId, GroupItem item) {
        GroupDao groupDao = DaoFactory.getGroupDao();
        Group group = groupDao.findByStringIdentifier(strId);
        if (group == null) {
            System.out.println("Creating group " + item.getName() + " ...");
            group = new Group(
                    strId,
                    item.getName());
            groupDao.create(group);
        }
        return group.getId();
    }
}
