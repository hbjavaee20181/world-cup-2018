package com.humanbooster.worldcup2018.exception;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class DataRefreshServiceException extends RuntimeException {

    public DataRefreshServiceException(Exception e) {
        super(e);
    }

    public DataRefreshServiceException(String message) {
        super(message);
    }
}
