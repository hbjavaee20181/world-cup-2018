package com.humanbooster.worldcup2018.dao;

import com.humanbooster.worldcup2018.entity.Team;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface TeamDao extends CrudDao<Team, Long> {
}
