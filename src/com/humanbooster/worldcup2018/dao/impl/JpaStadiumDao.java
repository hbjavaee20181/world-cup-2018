package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.StadiumDao;
import com.humanbooster.worldcup2018.entity.Stadium;

import javax.persistence.EntityManagerFactory;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaStadiumDao extends JpaCrudDao<Stadium, Long> implements StadiumDao {

    public JpaStadiumDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<Stadium> getTargetClass() {
        return Stadium.class;
    }
}
