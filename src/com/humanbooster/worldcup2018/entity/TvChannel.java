package com.humanbooster.worldcup2018.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "TV_CHANNELS")
public class TvChannel implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ICON")
    private String icon;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "ISO2")
    private String iso2;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "CHANNEL_LANGS", joinColumns = @JoinColumn(name = "TV_CHANNEL_ID", referencedColumnName = "ID"))
    private List<String> lang;

    @ManyToMany(mappedBy = "channels")
    private List<Match> matches;

    public TvChannel() {

    }

    public TvChannel(Long id, String name, String icon, String country, String iso2, List<String> lang) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.country = country;
        this.iso2 = iso2;
        this.lang = lang;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public List<String> getLang() {
        return lang;
    }

    public void setLang(List<String> lang) {
        this.lang = lang;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }
}
