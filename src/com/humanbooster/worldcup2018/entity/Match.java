package com.humanbooster.worldcup2018.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "MATCHS")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Match implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private Long name;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private MatchType type;

    @ManyToOne
    @JoinColumn(name = "HOME_TEAM_ID")
    private Team homeTeam;

    @ManyToOne
    @JoinColumn(name = "AWAY_TEAM_ID")
    private Team awayTeam;

    @Column(name = "HOME_RESULT")
    private Long homeResult;

    @Column(name = "AWAY_RESULT")
    private Long awayResult;


    @Column(name = "PLAY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "STADIUM_ID")
    private Stadium stadium;

    @ManyToMany
    @JoinTable(name = "MATCHS_CHANNELS")
    private List<TvChannel> channels;

    @Column(name = "FINISHED")
    private Boolean finished;

    @Column(name = "JOURNEY")
    private Integer matchDay;

    public Match() {

    }

    public Match(Long name, MatchType type, Team homeTeam, Team awayTeam, Long homeResult, Long awayResult,
                 Date date, Stadium stadium, List<TvChannel> channels, Boolean finished, Integer matchDay) {
        this.name = name;
        this.type = type;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeResult = homeResult;
        this.awayResult = awayResult;
        this.date = date;
        this.stadium = stadium;
        this.channels = channels;
        this.finished = finished;
        this.matchDay = matchDay;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getName() {
        return name;
    }

    public void setName(Long name) {
        this.name = name;
    }

    public MatchType getType() {
        return type;
    }

    public void setType(MatchType type) {
        this.type = type;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Long getHomeResult() {
        return homeResult;
    }

    public void setHomeResult(Long homeResult) {
        this.homeResult = homeResult;
    }

    public Long getAwayResult() {
        return awayResult;
    }

    public void setAwayResult(Long awayResult) {
        this.awayResult = awayResult;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }


    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public Integer getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(Integer matchday) {
        this.matchDay = matchday;
    }

    public List<TvChannel> getChannels() {
        return channels;
    }

    public void setChannels(List<TvChannel> channels) {
        this.channels = channels;
    }
}
