package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.FinalMatchDao;
import com.humanbooster.worldcup2018.entity.FinalMatch;

import javax.persistence.EntityManagerFactory;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaFinalMatchDao extends JpaCrudDao<FinalMatch, Long> implements FinalMatchDao {

    public JpaFinalMatchDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<FinalMatch> getTargetClass() {
        return FinalMatch.class;
    }
}
