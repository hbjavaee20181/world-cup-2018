package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018.dao.*;
import com.humanbooster.worldcup2018.entity.*;
import com.humanbooster.worldcup2018.util.StringUtils;
import com.humanbooster.worldcup2018consumer.model.KnockoutMatchItem;
import com.humanbooster.worldcup2018consumer.model.MatchItem;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class MatchService {

    public Long createGroupMatchIfNotExists(Long groupId, MatchItem item) {
        GroupDao groupDao = DaoFactory.getGroupDao();
        MatchDao matchDao = DaoFactory.getMatchDao();
        GroupMatchDao groupMatchDao = DaoFactory.getGroupMatchDao();
        TeamDao teamDao = DaoFactory.getTeamDao();
        StadiumDao stadiumDao = DaoFactory.getStadiumDao();
        TvChannelDao tvChannelDao = DaoFactory.getTvChannelDao();

        // Get the group
        Group group = groupDao.find(groupId);

        // Get home and away teams
        Team homeTeam = teamDao.find(item.getHomeTeam());
        Team awayTeam = teamDao.find(item.getAwayTeam());

        // Get stadium
        Stadium stadium = stadiumDao.find(item.getStadium());

        // Get Tv Channels
        List<TvChannel> channels = item.getChannels().stream()
                .map(tvChannelDao::find)
                .collect(Collectors.toList());

        GroupMatch match = (GroupMatch) matchDao.findByNameId(item.getName());
        if (match == null) {
            System.out.println("Creating group match " + item.getName() + " ...");
            match = new GroupMatch(
                    item.getName(),
                    MatchType.valueOf(item.getType().name()),
                    homeTeam,
                    awayTeam,
                    item.getHomeResult(),
                    item.getAwayResult(),
                    item.getDate(),
                    stadium,
                    channels,
                    item.getFinished(),
                    item.getMatchday(),
                    group
            );
            groupMatchDao.create(match);
        }
        return match.getId();
    }

    public Long createFinalMatch(String knockoutId, KnockoutMatchItem item) {
        RoundType type = RoundType.fromRep(knockoutId);

        MatchDao matchDao = DaoFactory.getMatchDao();
        FinalMatchDao finalMatchDao = DaoFactory.getFinalMatchDao();
        TeamDao teamDao = DaoFactory.getTeamDao();
        StadiumDao stadiumDao = DaoFactory.getStadiumDao();
        TvChannelDao tvChannelDao = DaoFactory.getTvChannelDao();

        // Get home and away teams
        Team homeTeam = null;
        Team awayTeam = null;
        Team winner = null;
        if (StringUtils.isNumeric(item.getHomeTeam())) {
            homeTeam = teamDao.find(Long.parseLong(item.getHomeTeam()));
        }
        if (StringUtils.isNumeric(item.getAwayTeam())) {
            awayTeam = teamDao.find(Long.parseLong(item.getAwayTeam()));
        }
        if (item.getWinner() != null) {
            winner = teamDao.find(item.getWinner());
        }

        // Get stadium
        Stadium stadium = stadiumDao.find(item.getStadium());

        // Get Tv Channels
        List<TvChannel> channels = item.getChannels().stream()
                .map(tvChannelDao::find)
                .collect(Collectors.toList());

        FinalMatch match = (FinalMatch) matchDao.findByNameId(item.getName());
        if (match == null) {
            System.out.println("Creating group match " + item.getName() + " ...");
            match = new FinalMatch(
                    item.getName(),
                    MatchType.valueOf(item.getType().name()),
                    homeTeam,
                    awayTeam,
                    item.getHomeResult(),
                    item.getAwayResult(),
                    item.getDate(),
                    stadium,
                    channels,
                    item.getFinished(),
                    item.getMatchday(),
                    item.getHomePenalty(),
                    item.getAwayPenalty(),
                    winner,
                    type
            );
            finalMatchDao.create(match);
        }
        return match.getId();
    }
}
