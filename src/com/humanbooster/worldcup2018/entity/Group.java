package com.humanbooster.worldcup2018.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "GROUPS")
public class Group implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "STR_ID")
    private String strIdentifier;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @OneToOne
    @JoinColumn(name = "WINNER_ID")
    private Team winner;

    @OneToOne
    @JoinColumn(name = "RUNNER_UP_ID")
    private Team runnerUp;

    @OneToMany(mappedBy = "group")
    private List<GroupMatch> matches = new ArrayList<>();

    public Group() {

    }

    public Group(String strIdentifier, String groupName) {
        this.strIdentifier = strIdentifier;
        this.groupName = groupName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrIdentifier() {
        return strIdentifier;
    }

    public void setStrIdentifier(String strIdentifier) {
        this.strIdentifier = strIdentifier;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String name) {
        this.groupName = name;
    }

    public Team getWinner() {
        return winner;
    }

    public void setWinner(Team winner) {
        this.winner = winner;
    }

    public Team getRunnerUp() {
        return runnerUp;
    }

    public void setRunnerUp(Team runnerUp) {
        this.runnerUp = runnerUp;
    }

    public List<GroupMatch> getMatches() {
        return matches;
    }

    public void setMatches(List<GroupMatch> matches) {
        this.matches = matches;
    }
}
