package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.TeamDao;
import com.humanbooster.worldcup2018.entity.Team;

import javax.persistence.EntityManagerFactory;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaTeamDao extends JpaCrudDao<Team, Long> implements TeamDao {


    public JpaTeamDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<Team> getTargetClass() {
        return Team.class;
    }
}
