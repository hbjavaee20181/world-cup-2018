package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.MatchDao;
import com.humanbooster.worldcup2018.entity.Match;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaMatchDao extends JpaCrudDao<Match, Long> implements MatchDao {

    public JpaMatchDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<Match> getTargetClass() {
        return Match.class;
    }

    @Override
    public Match findByNameId(Long name) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Match> q = em.createQuery("select m from Match AS m where m.name = :name", Match.class);
        q.setParameter("name", name);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
