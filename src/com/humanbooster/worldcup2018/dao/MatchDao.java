package com.humanbooster.worldcup2018.dao;

import com.humanbooster.worldcup2018.entity.Match;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface MatchDao extends CrudDao<Match, Long> {
    Match findByNameId(Long name);
}
