package com.humanbooster.worldcup2018.dao;

import com.humanbooster.worldcup2018.dao.impl.*;
import com.humanbooster.worldcup2018.manager.PersistenceManager;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class DaoFactory {

    private DaoFactory() {
    }

    public static StadiumDao getStadiumDao() {
        return new JpaStadiumDao(PersistenceManager.getEntityManagerFactory());
    }

    public static TvChannelDao getTvChannelDao() {
        return new JpaTvChannelDao(PersistenceManager.getEntityManagerFactory());
    }

    public static TeamDao getTeamDao() {
        return new JpaTeamDao(PersistenceManager.getEntityManagerFactory());
    }

    public static GroupDao getGroupDao() {
        return new JpaGroupDao(PersistenceManager.getEntityManagerFactory());
    }

    public static MatchDao getMatchDao() {
        return new JpaMatchDao(PersistenceManager.getEntityManagerFactory());
    }

    public static GroupMatchDao getGroupMatchDao() {
        return new JpaGroupMatchDao(PersistenceManager.getEntityManagerFactory());
    }

    public static FinalMatchDao getFinalMatchDao() {
        return new JpaFinalMatchDao(PersistenceManager.getEntityManagerFactory());
    }
}
