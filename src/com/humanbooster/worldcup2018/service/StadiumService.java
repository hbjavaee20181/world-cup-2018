package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018.dao.DaoFactory;
import com.humanbooster.worldcup2018.dao.StadiumDao;
import com.humanbooster.worldcup2018.entity.Stadium;
import com.humanbooster.worldcup2018consumer.model.StadiumItem;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class StadiumService {

    public Long createStadiumIfNotExists(StadiumItem item) {
        StadiumDao stadiumDao = DaoFactory.getStadiumDao();
        Stadium stadium = stadiumDao.find(item.getId());
        if (stadium == null) {
            System.out.println("Creating stadium " + item.getName() + " ...");
            stadium = new Stadium(
                    item.getId(),
                    item.getName(),
                    item.getCity(),
                    item.getLat(),
                    item.getLng(),
                    item.getImage());
            stadiumDao.create(stadium);
        }
        return stadium.getId();
    }
}
