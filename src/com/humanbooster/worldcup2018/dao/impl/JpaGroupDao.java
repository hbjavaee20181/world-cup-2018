package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.GroupDao;
import com.humanbooster.worldcup2018.entity.Group;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaGroupDao extends JpaCrudDao<Group, Long> implements GroupDao {

    public JpaGroupDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<Group> getTargetClass() {
        return Group.class;
    }

    @Override
    public Group findByStringIdentifier(String strId) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Group> q = em.createQuery("select g from Group AS g where g.strIdentifier = :strIdentifier", Group.class);
        q.setParameter("strIdentifier", strId);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
