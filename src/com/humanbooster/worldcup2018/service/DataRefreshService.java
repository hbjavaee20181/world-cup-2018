package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018consumer.model.GroupItem;
import com.humanbooster.worldcup2018consumer.model.KnockoutItem;
import com.humanbooster.worldcup2018consumer.model.WordCupJsonItem;
import com.humanbooster.worldcup2018consumer.service.WorldCupWebServiceConsumer;

import java.util.Map;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class DataRefreshService {

    public void refreshData() {
        WordCupJsonItem allData = WorldCupWebServiceConsumer.getInstance().getAllData();

        // Init services
        StadiumService stadiumService = new StadiumService();
        TvChannelService tvChannelService = new TvChannelService();
        TeamService teamService = new TeamService();
        GroupService groupService = new GroupService();
        MatchService matchService = new MatchService();

        // Update Stadiums
        System.out.println("Updating Stadiums ...");
        allData.getStadiums().forEach(stadiumService::createStadiumIfNotExists);

        // Update Tv Channels
        System.out.println("Updating Tv Channels ...");
        allData.getTvchannels().forEach(tvChannelService::createTvChannelIfNotExists);

        // Update Teams
        System.out.println("Updating Teams ...");
        allData.getTeams().forEach(teamService::createTeamIfNotExists);

        // Update Groups and Group matches
        System.out.println("Updating Groups and group matches...");
        for (Map.Entry<String, GroupItem> entry : allData.getGroups().entrySet()) {
            String groupStringIdentifier = entry.getKey();
            GroupItem groupItem = entry.getValue();

            // Create the group
            Long groupId = groupService.createGroupIfNotExists(groupStringIdentifier, groupItem);

            // Update group matches for the current group
            entry.getValue().getMatches().forEach((m) -> matchService.createGroupMatchIfNotExists(groupId, m));
        }

        // Update knockout matches
        System.out.println("Updating Knockout matches...");
        for (Map.Entry<String, KnockoutItem> entry : allData.getKnockout().entrySet()) {
            String knockoutIdentifier = entry.getKey();
            entry.getValue().getMatches().forEach(v -> matchService.createFinalMatch(knockoutIdentifier, v));
        }
    }
}
