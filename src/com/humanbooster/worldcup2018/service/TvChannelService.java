package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018.dao.DaoFactory;
import com.humanbooster.worldcup2018.dao.TvChannelDao;
import com.humanbooster.worldcup2018.entity.TvChannel;
import com.humanbooster.worldcup2018consumer.model.TvChannelItem;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class TvChannelService {

    public Long createTvChannelIfNotExists(TvChannelItem item) {
        TvChannelDao tvChannelDao = DaoFactory.getTvChannelDao();
        TvChannel channel = tvChannelDao.find(item.getId());
        if (channel == null) {
            System.out.println("Creating Tv channel " + item.getName() + " ...");
            channel = new TvChannel(
                    item.getId(),
                    item.getName(),
                    item.getIcon(),
                    item.getCountry(),
                    item.getIso2(),
                    item.getLang());
            tvChannelDao.create(channel);
        }
        return channel.getId();
    }
}
