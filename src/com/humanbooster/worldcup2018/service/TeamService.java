package com.humanbooster.worldcup2018.service;

import com.humanbooster.worldcup2018.dao.DaoFactory;
import com.humanbooster.worldcup2018.dao.TeamDao;
import com.humanbooster.worldcup2018.entity.Team;
import com.humanbooster.worldcup2018consumer.model.TeamItem;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class TeamService {

    public Long createTeamIfNotExists(TeamItem item) {
        TeamDao teamDao = DaoFactory.getTeamDao();
        Team team = teamDao.find(item.getId());
        if (team == null) {
            System.out.println("Creating team " + item.getName() + " ...");
            team = new Team(
                    item.getId(),
                    item.getName(),
                    item.getFifaCode(),
                    item.getIso2(),
                    item.getFlag(),
                    item.getEmoji());
            teamDao.create(team);
        }
        return team.getId();
    }
}
