package com.humanbooster.worldcup2018.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "FINAL_MATCHS")
public class FinalMatch extends Match {

    @Column(name = "HOME_PENALTY")
    private Long homePenalty;

    @Column(name = "AWAY_PENALTY")
    private Long awayPenalty;

    @ManyToOne
    @JoinColumn(name = "TEAM_FINAL_WINNER_ID")
    private Team finalWinner;

    @Enumerated(EnumType.STRING)
    private RoundType roundType;

    public FinalMatch() {

    }

    public FinalMatch(Long name, MatchType type, Team homeTeam, Team awayTeam, Long homeResult, Long awayResult,
                      Date date, Stadium stadium, List<TvChannel> channels, Boolean finished, Integer matchDay,
                      Long homePenalty, Long awayPenalty, Team finalWinner, RoundType roundType) {
        super(name, type, homeTeam, awayTeam, homeResult, awayResult, date, stadium, channels, finished, matchDay);
        this.homePenalty = homePenalty;
        this.awayPenalty = awayPenalty;
        this.finalWinner = finalWinner;
        this.roundType = roundType;
    }

    public Long getHomePenalty() {
        return homePenalty;
    }

    public void setHomePenalty(Long homePenalty) {
        this.homePenalty = homePenalty;
    }

    public Long getAwayPenalty() {
        return awayPenalty;
    }

    public void setAwayPenalty(Long awayPenalty) {
        this.awayPenalty = awayPenalty;
    }

    public Team getFinalWinner() {
        return finalWinner;
    }

    public void setFinalWinner(Team finalWinner) {
        this.finalWinner = finalWinner;
    }

    public RoundType getRoundType() {
        return roundType;
    }

    public void setRoundType(RoundType roundType) {
        this.roundType = roundType;
    }
}
