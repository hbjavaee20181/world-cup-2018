package com.humanbooster.worldcup2018.dao.impl;

import com.humanbooster.worldcup2018.dao.GroupMatchDao;
import com.humanbooster.worldcup2018.entity.GroupMatch;

import javax.persistence.EntityManagerFactory;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JpaGroupMatchDao extends JpaCrudDao<GroupMatch, Long> implements GroupMatchDao {

    public JpaGroupMatchDao(EntityManagerFactory emf) {
        super(emf);
    }

    @Override
    protected Class<GroupMatch> getTargetClass() {
        return GroupMatch.class;
    }

}
