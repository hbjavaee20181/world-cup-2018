package com.humanbooster.worldcup2018.servlet;

import com.humanbooster.worldcup2018.service.DataRefreshService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebServlet(urlPatterns = "/refresh")
public class RefreshServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DataRefreshService service = new DataRefreshService();
        service.refreshData();
        resp.sendRedirect(getServletContext().getContextPath() + "/home");
    }
}
